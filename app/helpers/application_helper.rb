module ApplicationHelper

  # alert => danger, notice => success
  def alert_class(message_type)
    case message_type
      when "alert"
        return "danger"
      when "notice"
        return "success"
      else
        return message_type
    end
  end
end
