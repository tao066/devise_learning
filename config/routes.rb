Rails.application.routes.draw do
  # static pages
  root to: 'static_pages#home'

  # users
  get '/users',  to: 'users#index'
  get '/mypage', to: 'users#mypage'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
